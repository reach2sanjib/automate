package bdd.automate;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
    }
	// Method to add 2 numbers
	public int addNumbers(int numberOne, int numberTwo) {
		return numberOne + numberTwo;
	}
	// Method to subtract 2 numbers
	public int subtractNumbers(int numberOne, int numberTwo) {
		return numberOne - numberTwo;
	}
}
